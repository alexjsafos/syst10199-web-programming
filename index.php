<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Slots</title>
</head>
<body>

    <?php
        $fruits = array('Apple',
        'Cherry','Grapes',
        'Lemon',
        'Orange', 'Pear',
         'Watermelon');

         $winner = array('Apple|Apple|Apple' => '10',
         'Cherry|Cherry|Cherry' => '20',
         'Grapes|Grapes|Grapes' => '30',
         'Lemon|Lemon|Lemon' => '40',
         'Orange|Orange|Orange' => '50',
         'Pear|Pear|Pear' => '60',
         'Watermelon|Watermelon|Watermelon' => '100',)

         $wheel1 = array();

         foreach($fruits as $element) {
             $wheel1[]=$element;
        }

        $wheel2 = array_reverse($wheel1);
        $wheel3 = $wheel1;

        if(isset($_POST['submit'])){
            $start1 = $stop1;
            $start2 = $stop2;
            $start3 = $stop3;
            $total = $_POST['total'];
            $bust = $_POST['bust'];
        } else {
            $start1 = 0;
            $start2 = 0;
            $start3 = 0;
            $total = 100;
            $bust = false;
        }

        if (isset($_POST['reset'])){
            $start1 = 0;
            $start2 = 0;
            $start3 = 0;
            $total = 100;
            $bust = false;
        }

        $stop1 = rand(count($wheel1) + $start1, 10 * count($wheel1)) % count($wheel1);
        $stop2 = rand(count($wheel1) + $start2, 10 * count($wheel1)) % count($wheel1);
        $stop3 = rand(count($wheel1) + $start3, 10 * count($wheel1)) % count($wheel1);

        $result1 = $wheel1[$stop1];
        $result2 = $wheel2[$stop2];
        $result3 = $wheel3[$stop3];

        $slot_result = $result1. '|' . $result2 . '|' . $result3;

    ?>
    
</body>
</html>